chrome.runtime.onMessage.addListener(function(request) {
	if(request.cmd == 'addContextMenu') {
		chrome.contextMenus.removeAll(function() {
			chrome.contextMenus.create({
				title: 'Copy SVG Code to Clipboard',
				contexts: ['all'],
				onclick: kc_SVG_copy_trigger,
			});
		});
	} else if(request.cmd == 'removeContextMenu') {
		chrome.contextMenus.removeAll();
	}
});

function kc_SVG_copy_trigger(info, tab) {
    chrome.tabs.sendMessage(tab.id, "getClickedEl");
}
